import pygame, thorpy
import sys, os
from card_game import loadParams

#python3 thorpy_test.py 790 540 120 156 /home/xol/Pygame/tripletriad/TripleTriad/02_tapis_triple_triad.jpg
WHITE = (255,255,255)

class FakeDeck(object):

       def __init__(self,r,int1,int2,nb,v,name,hollow,nbmax):
          self.r = r
          self.int1 = int1
          self.int2 = int2
          self.nb = nb
          self.v = v
          self.name= name
          self.hollow=hollow
          self.nbmax=nbmax

       def draw(self,screen,c):
          
           for i in range(0,self.nb):
             x=self.r.x
             y=self.r.y
             a, b = divmod(i, self.nbmax) 
             if(not self.v):
                  y=y+(a*self.r.height)+self.int2*a
             else:
                  x=x+(a*self.r.width)+self.int2*a
              
             if(not self.v):
               x=x+(b*self.r.width/self.int1)+self.int2*b
             else:
               y=y+(b*self.r.height/self.int1)+self.int2*b
             #print(str(x)+ ":" + str(y) + ":" + str(a) + ":" + str(b))
             rect = pygame.Rect(x,y,self.r.width,self.r.height)
             pygame.draw.rect(screen, c, rect) #delete old
             pygame.display.update(rect)

       def to_string(self):
          orientation=""
          hollow=""
          if self.v : orientation = "v" 
          else : orientation = "h" 
          if self.hollow : hollow = "h" 
          else : hollow = "x" 
          return self.name + ";" + str(self.r.x) + ";" + str(self.r.y) + ";" + str(self.r.width) + ";" + str(self.r.height) + ";" + str(self.int1) + ";" + str(self.int2) + ";" + orientation + ";" + hollow + ";" + str(self.nbmax)

       def from_string(line):
          elements = line.split(";")
          r = pygame.Rect(int(elements[1]),int(elements[2]),int(elements[3]),int(elements[4]))
          int1 = int(elements[5])
          int2 = int(elements[6])
          nb = int(elements[9])
          v =  True if elements[7] == "v" else False
          name= elements[0]
          hollow= True if elements[8] == "h" else False
          nbmax=int(elements[9])

          return FakeDeck(r,int1,int2,nb,v,name,hollow,nbmax)


b_save=False
b_load=False
b_delete=False
b_unselect=False

def save():
   global b_save
   b_save=True



def load():
   global b_load
   b_load=True


def delete():
   global b_delete
   b_delete=True

def unselect():
   global b_unselect
   b_unselect=True

def save_file(file_name, decks):
   basePath = os.getcwd()
   #file_name = "config/"+file_name
   file_absolute_name= os.path.join(basePath,file_name)
   print(file_absolute_name)
   fic = open(file_absolute_name,'w')
   for d in decks:
      fic.write(d.to_string() + '\n')
   fic.close

def load_file(file_name):
   basePath = os.getcwd()
   #file_name = "config/"+file_name
   file_absolute_name= os.path.join(basePath,file_name)
   print(file_absolute_name)
   fic = open(file_absolute_name,'r')
   decks = []
   for line in fic.readlines():
      d = FakeDeck.from_string(line)
      print(d.to_string())
      decks.append(d)
   fic.close
   return decks

general_params=loadParams("general_params.json")
pygame.init()
pygame.key.set_repeat(300, 30)
width = general_params["screen_width"]
menu_width = 200
height = general_params["screen_height"] + 10
screen = pygame.display.set_mode((width+menu_width,height))
img = None
picture=None
imagePathBack = general_params["background"]
img = pygame.image.load(imagePathBack).convert()
picture = pygame.transform.scale(img, (width, height-15))

screen.fill((255,255,255))
if img != None:
   screen.blit(picture,[0,0])

clock = pygame.time.Clock()

deck_liste= []

pygame.display.flip()



#declaration of some ThorPy elements ...

sliderw = thorpy.SliderX(100, (0, 200), "c. width")
sliderh = thorpy.SliderX(100, (0, 200), "c. height")
sliderm = thorpy.SliderX(7, (1, 8), "interval")
sliderm2 = thorpy.SliderX(19, (1, 20), "interval2")
sliderc = thorpy.SliderX(9, (1, 10), "nb cards")
checker_check = thorpy.Checker("vertical")
checker_hollow = thorpy.Checker("hollow")
slidermax = thorpy.SliderX(9, (1, 10), "nb max")
button = thorpy.make_button("Save", func=save)
buttonLoad = thorpy.make_button("Load", func=load)
buttonDelete = thorpy.make_button("Delete", func=delete)
buttonUnselect = thorpy.make_button("Unselect", func=unselect)
e_counter =  thorpy.Inserter(name="Name:")
fileName =  thorpy.Inserter(name="File:")
fileName.set_value(general_params["fic_config"])
sliderw.set_value(general_params["card_width"])
sliderh.set_value(general_params["card_height"])

box = thorpy.Box(elements=[sliderw,sliderh,sliderm,sliderm2,sliderc,checker_check,checker_hollow,slidermax,e_counter,buttonUnselect,buttonDelete,fileName,buttonLoad,button])
#we regroup all elements on a menu, even if we do not launch the menu
menu = thorpy.Menu(box)
#important : set the screen as surface for all elements
for element in menu.get_population():
    element.surface = screen
#use the elements normally...
box.set_topleft((width,0))
box.blit()
box.update()


rect = pygame.Rect(width,0,2,height)
pygame.draw.rect(screen, (0,0,0), rect) #delete old
pygame.display.update(rect)

rect = pygame.Rect(width/2,0,2,height)
pygame.draw.rect(screen, (0,0,0), rect) #delete old
pygame.display.update(rect)

rect = pygame.Rect(0,height/2,width,2)
pygame.draw.rect(screen, (0,0,0), rect) #delete old
pygame.display.update(rect)

rect = pygame.Rect(0,height-15,width,2)
pygame.draw.rect(screen, (0,0,0), rect) #delete old
pygame.display.update(rect)

change = False
playing_game = True
selected_deck = None

while playing_game:
    clock.tick(45)
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            playing_game = False
            break
        elif event.type == pygame.MOUSEBUTTONUP:
            pos = pygame.mouse.get_pos()
            current_deck = None
            if selected_deck is None:
               for d in reversed(deck_liste):
                   if(d.r.collidepoint(pos[0],pos[1])) :
                    #liste_tmp.append(d)
                       current_deck = d
                       selected_deck = d
                       #int1,int2,nb,v,name,hollow,nbmax
                       sliderm.set_value((current_deck.int1))
                       sliderm2.set_value((current_deck.int2))
                       sliderw.set_value((current_deck.r.width))
                       sliderh.set_value((current_deck.r.height))
                       sliderc.set_value((current_deck.nb))
                       checker_check.set_value((current_deck.v))
                       e_counter.set_value(current_deck.name)
                       #e_counter.refresh()
                       checker_hollow.set_value((current_deck.hollow))
                       slidermax.set_value((current_deck.nbmax))
                       change = True
                       continue
                    
            x = pos[0] - sliderw.get_value() / 2
            y = pos[1] - sliderh.get_value() / 2
            if current_deck is None and x < width - sliderw.get_value() and sliderw.get_value() > 0 and sliderh.get_value() > 0:
                rectangle = pygame.Rect(x,y,sliderw.get_value(),sliderh.get_value())
                deck = FakeDeck(rectangle,int(sliderm.get_value()),int(sliderm2.get_value()),int(sliderc.get_value()),int(checker_check.get_value()),e_counter.get_value(),checker_hollow.get_value(),int(slidermax.get_value()))
                if selected_deck is not None :
                   deck_liste[deck_liste.index(selected_deck)] = deck
                   selected_deck= None
                else :
                   deck_liste.append(deck)
                print(deck.to_string())
                change = True
                #deck.draw(screen, (0,0,0))
        menu.react(event) #the menu automatically integrate your elements

    if b_unselect :
       selected_deck = None
       change = True
       b_unselect = False

    if b_delete :
       if selected_deck is not None :
           deck_liste.remove(selected_deck) 
           selected_deck = None
           change = True
       b_delete = False

    if b_save:
        save_file(fileName.get_value()+general_params["extension_fic_config"], deck_liste)
        b_save = False

    if b_load:
        deck_liste = load_file(fileName.get_value()+general_params["extension_fic_config"])
        b_load = False
        change = True

    if change :
       
       if img != None:
           screen.blit(picture,[0,0])
       else:
         rect = pygame.Rect(0,0,width,height)
         pygame.draw.rect(screen, (255,255,255), rect) #delete old
         pygame.display.update(rect)

       rect = pygame.Rect(width,0,2,height)
       pygame.draw.rect(screen, (0,0,0), rect) #delete old
       pygame.display.update(rect)

       rect = pygame.Rect(width/2,0,2,height)
       pygame.draw.rect(screen, (0,0,0), rect) #delete old
       pygame.display.update(rect)

       rect = pygame.Rect(0,height/2,width,2)
       pygame.draw.rect(screen, (0,0,0), rect) #delete old
       pygame.display.update(rect)

       rect = pygame.Rect(0,height-15,width,2)
       pygame.draw.rect(screen, (0,0,0), rect) #delete old
       pygame.display.update(rect)

       font = pygame.font.Font(None,20)
       for d in deck_liste:
          c = (0,100,100) if d == selected_deck else (0,0,0)
          d.draw(screen,c) 
          text = font.render(d.name,True,WHITE)
          screen.blit(text,[d.r.x,d.r.y])
       pygame.display.flip()
       change = False
    

pygame.quit()
