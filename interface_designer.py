import pygame, thorpy
import sys, os
from card_game import loadParams

#python3 thorpy_test.py 790 540 120 156 /home/xol/Pygame/tripletriad/TripleTriad/02_tapis_triple_triad.jpg

class FakeDeck(object):

       def __init__(self,r,int1,int2,nb,v,name,hollow,nbmax):
          self.r = r
          self.int1 = int1
          self.int2 = int2
          self.nb = nb
          self.v = v
          self.name= name
          self.hollow=hollow
          self.nbmax=nbmax

       def draw(self,screen,c):
          
           for i in range(0,self.nb):
             x=self.r.x
             y=self.r.y
             a, b = divmod(i, self.nbmax) 
             if(not self.v):
                  y=y+(a*self.r.height)+self.int2*a
             else:
                  x=x+(a*self.r.width)+self.int2*a
              
             if(not self.v):
               x=x+(b*self.r.width/self.int1)+self.int2*b
             else:
               y=y+(b*self.r.height/self.int1)+self.int2*b
             #print(str(x)+ ":" + str(y) + ":" + str(a) + ":" + str(b))
             rect = pygame.Rect(x,y,self.r.width,self.r.height)
             pygame.draw.rect(screen, c, rect) #delete old
             pygame.display.update(rect)

       def to_string(self):
          orientation=""
          hollow=""
          if self.v : orientation = "v" 
          else : orientation = "h" 
          if self.hollow : hollow = "h" 
          else : hollow = "x" 
          return self.name + ";" + str(self.r.x) + ";" + str(self.r.y) + ";" + str(self.r.width) + ";" + str(self.r.height) + ";" + str(self.int1) + ";" + str(self.int2) + ";" + orientation + ";" + hollow + ";" + str(self.nbmax)


b_save=False

def save():
   global b_save
   b_save=True

def save_file(file_name, decks):
   basePath = os.path.dirname(__file__)
   file_name = "config/"+file_name
   file_absolute_name= os.path.join(basePath,file_name+".ccfg")
   print(file_absolute_name)
   fic = open(file_absolute_name,'w')
   for d in decks:
      fic.write(d.to_string() + '\n')
   fic.close

general_params=loadParams("general_params.json")
pygame.init()
pygame.key.set_repeat(300, 30)
width = general_params["screen_width"]
menu_width = 200
height = general_params["screen_height"]
screen = pygame.display.set_mode((width+menu_width,height))
img = None
picture=None
imagePathBack = general_params["background"]
img = pygame.image.load(imagePathBack).convert()
picture = pygame.transform.scale(img, (width, height-15))

screen.fill((255,255,255))
if img != None:
   screen.blit(picture,[0,0])

clock = pygame.time.Clock()

deck_liste= []

pygame.display.flip()



#declaration of some ThorPy elements ...

sliderw = thorpy.SliderX(100, (0, 200), "c. width")
sliderh = thorpy.SliderX(100, (0, 200), "c. height")
sliderm = thorpy.SliderX(7, (1, 8), "interval")
sliderm2 = thorpy.SliderX(19, (1, 20), "interval2")
sliderc = thorpy.SliderX(9, (1, 10), "nb cards")
checker_check = thorpy.Checker("vertical")
checker_hollow = thorpy.Checker("hollow")
slidermax = thorpy.SliderX(9, (1, 10), "nb max")
button = thorpy.make_button("Save", func=save)
e_counter =  thorpy.Inserter(name="Name:")
fileName =  thorpy.Inserter(name="File:")
sliderw.set_value(general_params["card_width"])
sliderh.set_value(general_params["card_height"])

box = thorpy.Box(elements=[sliderw,sliderh,sliderm,sliderm2,sliderc,checker_check,checker_hollow,slidermax,button,e_counter,fileName])
#we regroup all elements on a menu, even if we do not launch the menu
menu = thorpy.Menu(box)
#important : set the screen as surface for all elements
for element in menu.get_population():
    element.surface = screen
#use the elements normally...
box.set_topleft((width,0))
box.blit()
box.update()


rect = pygame.Rect(width,0,2,height)
pygame.draw.rect(screen, (0,0,0), rect) #delete old
pygame.display.update(rect)

rect = pygame.Rect(width/2,0,2,height)
pygame.draw.rect(screen, (0,0,0), rect) #delete old
pygame.display.update(rect)

rect = pygame.Rect(0,height/2,width,2)
pygame.draw.rect(screen, (0,0,0), rect) #delete old
pygame.display.update(rect)

rect = pygame.Rect(0,height-15,width,2)
pygame.draw.rect(screen, (0,0,0), rect) #delete old
pygame.display.update(rect)

change = False
playing_game = True
while playing_game:
    clock.tick(45)
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            playing_game = False
            break
        elif event.type == pygame.MOUSEBUTTONUP:
            pos = pygame.mouse.get_pos()
            liste_tmp = []
            for d in reversed(deck_liste):
                if(d.r.collidepoint(pos[0],pos[1])) :
                    liste_tmp.append(d)
            for d in liste_tmp:
                deck_liste.remove(d)
                change = True
                #d.draw(screen, (255,255,255))

            if len(liste_tmp) == 0 and pos[0] < width - sliderw.get_value() and sliderw.get_value() > 0 and sliderh.get_value() > 0:
                rectangle = pygame.Rect(pos[0],pos[1],sliderw.get_value(),sliderh.get_value())
                deck = FakeDeck(rectangle,int(sliderm.get_value()),int(sliderm2.get_value()),int(sliderc.get_value()),int(checker_check.get_value()),e_counter.get_value(),checker_hollow.get_value(),int(slidermax.get_value()))
                deck_liste.append(deck)
                print(deck.to_string())
                change = True
                #deck.draw(screen, (0,0,0))
        menu.react(event) #the menu automatically integrate your elements

    if b_save:
        save_file(fileName.get_value(), deck_liste)
        b_save = False

    if change :
       
       if img != None:
           screen.blit(picture,[0,0])
       else:
         rect = pygame.Rect(0,0,width,height)
         pygame.draw.rect(screen, (255,255,255), rect) #delete old
         pygame.display.update(rect)

       rect = pygame.Rect(width,0,2,height)
       pygame.draw.rect(screen, (0,0,0), rect) #delete old
       pygame.display.update(rect)

       rect = pygame.Rect(width/2,0,2,height)
       pygame.draw.rect(screen, (0,0,0), rect) #delete old
       pygame.display.update(rect)

       rect = pygame.Rect(0,height/2,width,2)
       pygame.draw.rect(screen, (0,0,0), rect) #delete old
       pygame.display.update(rect)

       rect = pygame.Rect(0,height-15,width,2)
       pygame.draw.rect(screen, (0,0,0), rect) #delete old
       pygame.display.update(rect)

       for d in deck_liste:
          d.draw(screen,(0,0,0)) 
       pygame.display.flip()
       change = False
    

pygame.quit()
