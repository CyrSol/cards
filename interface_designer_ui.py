import pygame, thorpy
import sys, os
from card_game import loadParams

#python3 thorpy_test.py 790 540 120 156 /home/xol/Pygame/tripletriad/TripleTriad/02_tapis_triple_triad.jpg
WHITE = (255,255,255)

class FakeDeck(object):

       def __init__(self,r,int1,int2,nb,v,name,hollow,nbmax):
          self.r = r
          self.int1 = int1
          self.int2 = int2
          self.nb = nb
          self.v = v
          self.name= name
          self.hollow=hollow
          self.nbmax=nbmax

       def draw(self,screen,c):
          
           for i in range(0,self.nb):
             x=self.r.x
             y=self.r.y
             a, b = divmod(i, self.nbmax) 
             if(not self.v):
                  y=y+(a*self.r.height)+self.int2*a
             else:
                  x=x+(a*self.r.width)+self.int2*a
              
             if(not self.v):
               x=x+(b*self.r.width/self.int1)+self.int2*b
             else:
               y=y+(b*self.r.height/self.int1)+self.int2*b
             #print(str(x)+ ":" + str(y) + ":" + str(a) + ":" + str(b))
             rect = pygame.Rect(x,y,self.r.width,self.r.height)
             pygame.draw.rect(screen, c, rect) #delete old
             pygame.display.update(rect)

       def to_string(self):
          orientation=""
          hollow=""
          if self.v : orientation = "v" 
          else : orientation = "h" 
          if self.hollow : hollow = "h" 
          else : hollow = "x" 
          return self.name + ";" + str(self.r.x) + ";" + str(self.r.y) + ";" + str(self.r.width) + ";" + str(self.r.height) + ";" + str(self.int1) + ";" + str(self.int2) + ";" + orientation + ";" + hollow + ";" + str(self.nbmax)

       def from_string(line):
          elements = line.split(";")
          r = pygame.Rect(int(elements[1]),int(elements[2]),int(elements[3]),int(elements[4]))
          int1 = int(elements[5])
          int2 = int(elements[6])
          nb = int(elements[9])
          v =  True if elements[7] == "v" else False
          name= elements[0]
          hollow= True if elements[8] == "h" else False
          nbmax=int(elements[9])

          return FakeDeck(r,int1,int2,nb,v,name,hollow,nbmax)


class FakeUIElement(object):

       def __init__(self,r,var,font,size,v,re,g,b,typele,interval,text=""):
         self.var=var
         self.font=font
         self.size=size
         self.v = v
         self.r=r
         self.re=re
         self.g=g
         self.b=b
         self.interval=interval
         self.typele=typele
         self.nb = 3 if typele == "liste" else 1
         self.text = text if text != "" else "X"

       def draw(self,screen):
           c = (self.re,self.g,self.b)
           for i in range(0,self.nb):
             x=self.r.x
             y=self.r.y
             
             if(self.v):
                  y=y+(i*self.r.height)+self.interval*i
             else:
                  x=x+(i*self.r.width)+self.interval*i
              
             #print(str(x)+ ":" + str(y) + ":" + str(a) + ":" + str(b))
             font = pygame.font.SysFont(self.font,self.size)
             text = font.render(self.text if self.typele != "constante"else self.var,True,c)
             screen.blit(text,[x,y])
             #rect = pygame.Rect(x,y,self.r.width,self.r.height)
             #pygame.draw.rect(screen, c, rect) #delete old
             #pygame.display.update(rect)


       def to_string(self):
          orientation=""
          hollow=""
          if self.v : orientation = "v" 
          else : orientation = "h" 
          
          return self.var + ";" + self.font + ";" + str(self.size) + ";" + orientation + ";" + str(self.r.x) + ";" + str(self.r.y) + ";" + str(self.r.width) + ";" + str(self.r.height) + ";" + str(self.re) + ";" + str(self.g) + ";" + str(self.b) + ";"  + self.typele  + ";" + str(self.interval)

       def from_string(line):
          elements = line.split(";")
          r = pygame.Rect(int(elements[4]),int(elements[5]),int(elements[6]),int(elements[7]))
          var= elements[0]
          font = elements[1]
          size = int(elements[2])
          v =  True if elements[3] == "v" else False
          re = int(elements[8])
          g=int(elements[9])
          b=int(elements[10])
          typele= elements[11]
          interval=int(elements[12])

          return FakeUIElement(r,var,font,size,v,re,g,b,typele,interval)

b_save=False
b_load=False
b_delete=False
b_unselect=False
b_update=False

def save():
   global b_save
   b_save=True



def load():
   global b_load
   b_load=True


def delete():
   global b_delete
   b_delete=True

def unselect():
   global b_unselect
   b_unselect=True

def update():
   global b_update
   b_update=True

def save_file(file_name, decks):
   basePath = os.getcwd()
   #file_name = "config/"+file_name
   file_absolute_name= os.path.join(basePath,file_name)
   print(file_absolute_name)
   fic = open(file_absolute_name,'w')
   for d in decks:
      fic.write(d.to_string() + '\n')
   fic.close

def load_file(file_name):
   basePath = os.getcwd()
   #file_name = "config/"+file_name
   file_absolute_name= os.path.join(basePath,file_name)
   print(file_absolute_name)
   fic = open(file_absolute_name,'r')
   decks = []
   for line in fic.readlines():
      d = FakeDeck.from_string(line)
      print(d.to_string())
      decks.append(d)
   fic.close
   return decks

def load_file_uie(file_name):
   basePath = os.getcwd()
   #file_name = "config/"+file_name
   file_absolute_name= os.path.join(basePath,file_name)
   print(file_absolute_name)
   fic = open(file_absolute_name,'r')
   decks = []
   for line in fic.readlines():
      d = FakeUIElement.from_string(line)
      print(d.to_string())
      decks.append(d)
   fic.close
   return decks

general_params=loadParams("general_params.json")
game_params=loadParams(general_params["game_params"])
pygame.init()
pygame.key.set_repeat(300, 30)
width = general_params["screen_width"]
menu_width = 200
height = general_params["screen_height"] + 10
screen = pygame.display.set_mode((width+menu_width,height))
img = None
picture=None
imagePathBack = general_params["background"]
img = pygame.image.load(imagePathBack).convert()
picture = pygame.transform.scale(img, (width, height-15))

screen.fill((255,255,255))
if img != None:
   screen.blit(picture,[0,0])

clock = pygame.time.Clock()


pygame.display.flip()



#declaration of some ThorPy elements ...


e_counter_font =  thorpy.Inserter(name="Font :")
e_counter_var =  thorpy.Inserter(name="Var :", size=(menu_width-45, 20))
e_counter_text =  thorpy.Inserter(name="Texte :")
sliders = thorpy.SliderX(50, (0, 75), "size")
checker_v = thorpy.Checker("vertical")
checker_v.set_value(1)
sliderr = thorpy.SliderX(100, (0, 255), "R")
sliderg = thorpy.SliderX(100, (0, 225), "G")
sliderb = thorpy.SliderX(100, (0, 255), "B")
sliderInt = thorpy.SliderX(50, (0, 50), "Interval")
e_counter_type =  thorpy.Inserter(name="Type :")

button = thorpy.make_button("Save", func=save)
buttonLoad = thorpy.make_button("Load", func=load)
buttonDelete = thorpy.make_button("Delete", func=delete)
buttonUnselect = thorpy.make_button("Unselect", func=unselect)
buttonUpdate = thorpy.make_button("Update", func=update)

fileName =  thorpy.Inserter(name="File:")
fileName.set_value(general_params["fic_interface"])
nbPlayers =  thorpy.Inserter(name="Nb players:")
nbPlayers.set_value(str(game_params["nb_players"]))
e_counter_font.set_value("arial")
e_counter_var.set_value("var                                ")
e_counter_type.set_value("element")
e_counter_text.set_value("XXXX")
sliders.set_value(15)

nbPlayersText=""
if (general_params["multi_fic_config"]):
   nbPlayersText=nbPlayers.get_value()
deck_liste = load_file(fileName.get_value()+nbPlayersText+general_params["extension_fic_config"])
uie_liste = load_file_uie(fileName.get_value()+nbPlayersText+general_params["extension_fic_interface"])

def getUIElement(x,y):
 var = e_counter_var.get_value()
 font = e_counter_font.get_value()
 size = int(sliders.get_value())
 v = int(checker_v.get_value())
 re = int(sliderr.get_value())
 g = int(sliderg.get_value())
 b = int(sliderb.get_value())
 typele = e_counter_type.get_value()
 interval = int(sliderInt.get_value())
 text = e_counter_text.get_value()
 fonte = pygame.font.SysFont(font,size)
 text_width, text_height = fonte.size(text)
 rectangle = pygame.Rect(x,y,text_width,text_height)

 return FakeUIElement(rectangle,var,font,size,v,re,g,b,typele,interval,text)

box = thorpy.Box(elements=[e_counter_font,e_counter_var,e_counter_text,sliders,checker_v,sliderr,sliderg,sliderb,sliderInt,e_counter_type,buttonUpdate,buttonUnselect,buttonDelete,fileName,nbPlayers,buttonLoad,button], size=(menu_width,height))
#we regroup all elements on a menu, even if we do not launch the menu
menu = thorpy.Menu(box)
#important : set the screen as surface for all elements
for element in menu.get_population():
    element.surface = screen
#use the elements normally...
box.set_topleft((width,0))
box.blit()
box.update()


rect = pygame.Rect(width,0,2,height)
pygame.draw.rect(screen, (0,0,0), rect) #delete old
pygame.display.update(rect)

rect = pygame.Rect(width/2,0,2,height)
pygame.draw.rect(screen, (0,0,0), rect) #delete old
pygame.display.update(rect)

rect = pygame.Rect(0,height/2,width,2)
pygame.draw.rect(screen, (0,0,0), rect) #delete old
pygame.display.update(rect)

rect = pygame.Rect(0,height-15,width,2)
pygame.draw.rect(screen, (0,0,0), rect) #delete old
pygame.display.update(rect)

change = True
playing_game = True
selected_uie = None

while playing_game:
    clock.tick(45)
    moved = False
    m_x = 0
    m_y = 0
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            playing_game = False
            break
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE : unselect()
            elif event.key == pygame.K_x : delete()
            elif event.key == pygame.K_DOWN : 
                moved = True
                m_y = 1 
            elif event.key == pygame.K_UP : 
                moved = True
                m_y = -1 
            elif event.key == pygame.K_RIGHT : 
                moved = True
                m_x = 1 
            elif event.key == pygame.K_LEFT : 
                moved = True
                m_x	 = -1 
                 
        elif event.type == pygame.MOUSEBUTTONUP:
            pos = pygame.mouse.get_pos()
            current_uie = None
            if selected_uie is None:
               for d in reversed(uie_liste):
                   if(d.r.collidepoint(pos[0],pos[1])) :
                    #liste_tmp.append(d)
                       current_uie = d
                       selected_uie = d
                       #int1,int2,nb,v,name,hollow,nbmax
                       e_counter_var.set_value(d.var)
                       e_counter_font.set_value(d.font)
                       sliders.set_value((d.size))
                       checker_v.set_value((d.v))
                       sliderr.set_value((d.re))
                       sliderg.set_value((d.g))
                       sliderb.set_value((d.b))
                       e_counter_type.set_value(d.typele)
                       sliderInt.set_value((d.interval))
                       e_counter_text.set_value(d.text)
                       change = True
                       box.blit()
                       box.update()
                       continue
            
            x = pos[0]
            y = pos[1]
            if x < width and current_uie is None:

                uiElement = getUIElement(x-5,y-10)
                if selected_uie is not None :
                   uie_liste[uie_liste.index(selected_uie)] = uiElement
                else :
                   uie_liste.append(uiElement)
                print(uiElement.to_string())
                selected_uie= uiElement
                change = True
                #deck.draw(screen, (0,0,0))
        menu.react(event) #the menu automatically integrate your elements

    if moved:
       if selected_uie is not None :
        selected_uie.r.y = selected_uie.r.y + m_y
        selected_uie.r.x = selected_uie.r.x + m_x
        change = True

    if b_update :
       if selected_uie is not None : 
          change = True
          uiElement = getUIElement(selected_uie.r.x,selected_uie.r.y)
          uie_liste[uie_liste.index(selected_uie)] = uiElement
          selected_uie = uiElement
       b_update= False

       #selected_uie = None
    #if selected_uie is not None : 
    #   uiElement = getUIElement(selected_uie.r.x,selected_uie.r.y)
    #   uie_liste[uie_liste.index(selected_uie)] = uiElement
    #   selected_uie = uiElement
    #   change = True

    if b_unselect :
       selected_uie = None
       change = True
       b_unselect = False

    if b_delete :
       if selected_uie is not None :
           uie_liste.remove(selected_uie) 
           selected_uie = None
           change = True
       b_delete = False

    if b_save:
        if (general_params["multi_fic_config"]):
           nbPlayersText=nbPlayers.get_value()
        save_file(fileName.get_value()+nbPlayersText+general_params["extension_fic_interface"], uie_liste)
        b_save = False

    if b_load:
        if (general_params["multi_fic_config"]):
           nbPlayersText=nbPlayers.get_value()
        deck_liste = load_file(fileName.get_value()+nbPlayersText+general_params["extension_fic_config"])
        uie_liste = load_file_uie(fileName.get_value()+nbPlayersText+general_params["extension_fic_interface"])
        b_load = False
        e_counter_var.set_value("var")
        change = True

    if change :
       
       if img != None:
           screen.blit(picture,[0,0])
       else:
         rect = pygame.Rect(0,0,width,height)
         pygame.draw.rect(screen, (255,255,255), rect) #delete old
         pygame.display.update(rect)

       rect = pygame.Rect(width,0,2,height)
       pygame.draw.rect(screen, (0,0,0), rect) #delete old
       pygame.display.update(rect)

       rect = pygame.Rect(width/2,0,2,height)
       pygame.draw.rect(screen, (0,0,0), rect) #delete old
       pygame.display.update(rect)

       rect = pygame.Rect(0,height/2,width,2)
       pygame.draw.rect(screen, (0,0,0), rect) #delete old
       pygame.display.update(rect)

       rect = pygame.Rect(0,height-15,width,2)
       pygame.draw.rect(screen, (0,0,0), rect) #delete old
       pygame.display.update(rect)

       font = pygame.font.Font(None,20)
       for d in deck_liste:
          c = (0,100,100) if d == selected_uie else (0,0,0)
          d.draw(screen,c) 
          text = font.render(d.name,True,WHITE)
          screen.blit(text,[d.r.x,d.r.y])
       if selected_uie is not None:
          pygame.draw.rect(screen, (0,100,100), selected_uie.r)
       for e in uie_liste:
          e.draw(screen)
       pygame.display.flip()
       change = False
    

pygame.quit()
